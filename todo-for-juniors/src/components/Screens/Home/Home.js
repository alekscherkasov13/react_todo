import React from 'react'
import Header from './Header'
import Todoitem from './Item/Todoitem'
import CreateTodoField from './Create-todo-field/CreateTodoField'
import { useState } from 'react'

const data = [
    {
        _id: "fghujioklp",
        title: "To invite BBC learning English",
        isCompleted: false
    },
    {
        _id: "bnb45452",
        title: "To go for a walk",
        isCompleted: false
    },
    {
        _id: "fghujib4334",
        title: "To develop todos aplication",
        isCompleted: false
    },

]
function Home() {
    const [todos, setTodos] = useState(data);

    const changeTodo = (id) => {
        const copy = [...todos];
        const current = copy.find(t => t._id === id);
        current.isCompleted = !current.isCompleted;
        setTodos(copy);
    }

    const removeTodo = (id) => {
        setTodos([...todos].filter(el => el._id !== id));
    }

    const addTodo = (title) => {
        console.log('yes')
        let copy = [...todos];
        console.log(copy);
        let bar = {
            _id: Date.now().toString(),
            title,
            isCompleted: false
        }
        copy.push(bar);
        setTodos(copy);
        console.log(todos);
    }

    return (
        <div className='bg-gray-900 h-screen text-white  mx-auto w-4/6 pr-3'>
            <Header />
            {todos.map((todo) =>
                <Todoitem key={todo._id}
                    todo={todo}
                    changeTodo={changeTodo}
                    removeTodo={removeTodo} />
            )}
            <CreateTodoField addTodo={addTodo} />
        </div>
    )
}

export default Home
