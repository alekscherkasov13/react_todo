import React from 'react'

function Header() {
    return (
        <div className='text-2xl font-bold text-center mb-4'>
            <h1>Todo for every day</h1>
        </div>
    )
}

export default Header
