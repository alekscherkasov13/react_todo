import React from 'react'
import Check from './Check'
import RemoveTodo from './RemoveTodo'


function Todoitem({ todo, changeTodo, removeTodo }) {
    //console.log(todo._id)
    return (
        <div className='flex items-center justify-between mb-4 rounded-2xl p-5 bg-zinc-700 w-full'>

            <button className='flex items-center' onClick={() => changeTodo(todo._id)}>
                <Check isCompleted={todo.isCompleted} />
                <span className={`${todo.isCompleted ? 'line-through' : ''}`}>{todo.title}
                </span>
            </button>
            <button onClick={() => removeTodo(todo._id)} >
                <RemoveTodo />
            </button>

        </div>
    )
}

export default Todoitem
