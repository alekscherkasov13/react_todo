import React from 'react'

import { BsTrash } from 'react-icons/bs'

function RemoveTodo() {
    return (
        <div size={22} className='text-grey-600 hover:text-red-500'>
            <BsTrash />
        </div>
    )
}

export default RemoveTodo
