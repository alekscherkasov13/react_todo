import React from 'react'
import { BsCheck } from 'react-icons/bs'


function Check({ isCompleted }) {
    //console.log(isCompleted)
    return (

        <div className={`border-2  rounded-lg ${isCompleted ? 'bg-pink-400 td' : ''} border-pink-500 w-7 h-7 mr-2 flex items-center justify-center`} >
            {isCompleted && <BsCheck size={24} className='text-gray-900' />}
        </div >
    )
}

export default Check
