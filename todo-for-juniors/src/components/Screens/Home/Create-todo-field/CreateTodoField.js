import React from 'react'
import { useState } from 'react'

function CreateTodoField({ addTodo }) {
    const [title, settitle] = useState('');
    return (
        <div>
            < div className='flex items-center mb-4 rounded-2xl p-3 border-2 mt-5 border-zinc-700 w-full justify-between' >
                <input className=' w-1/2 bg-transparent border-none outline-none' type="text" onChange={event => settitle(event.target.value)}
                    value={title}
                    onKeyPress={e => e.key === 'Enter' && addTodo(title)}
                    placeholder='Enter Todo'
                />
                <button onClick={() => addTodo(title)} className='ml-4 bg-pink-400 p-1 px-2 rounded-xl'>Add todo</button>
            </ div>
        </div>
    )
}

export default CreateTodoField

